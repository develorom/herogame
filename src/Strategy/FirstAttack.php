<?php

namespace HeroGame\Strategy;

use HeroGame\Model\CharacterInterface;
use HeroGame\Model\Collection\CollectionInterface;

interface FirstAttack
{
    /**
     * @param CollectionInterface $characters
     */
    public function setCharactersState(CollectionInterface $characters): void;
}