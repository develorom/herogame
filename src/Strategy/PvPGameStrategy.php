<?php

namespace HeroGame\Strategy;

use HeroGame\Enum\CharacterEnum;
use HeroGame\Enum\SkillEnum;
use HeroGame\Logger\ConsoleLogger;
use HeroGame\Model\CharacterInterface;
use HeroGame\Model\Collection\CollectionInterface;
use HeroGame\Model\Skill\Skill;

class PvPGameStrategy implements GameStrategyInterface
{
    private CollectionInterface $characters;
    private ConsoleLogger $consoleLogger;
    private int $maxTurns;

    private CharacterInterface $attacker;
    private CharacterInterface $defender;

    /**
     * PvPGameStrategy constructor.
     * @param CollectionInterface $characters
     */
    public function __construct(CollectionInterface $characters, int $maxTurns)
    {
        $this->characters = $characters;
        $this->consoleLogger = new ConsoleLogger();
        $this->maxTurns = $maxTurns;
    }

    /**
     * @throws \HeroGame\Exception\InvalidMoveException
     */
    public function runBattle(): void
    {
        $winnerDeclared = false;

        for ($i = 1; $i <= $this->maxTurns; $i++) {
            // todo fix typehint by creating a CharacterCollection which returns CharacterInterface
            $this->attacker = $this->characters->getFirstByFieldValue('getState', CharacterEnum::ATTACK);
            $this->defender = $this->characters->getFirstByFieldValue('getState', CharacterEnum::DEFEND);
            $this->consoleLogger->logInfo('= Round '.$i.' started');
            if ($i === 0) {
                $this->consoleLogger->logInfo($this->attacker->getName(). ' health:'.$this->attacker->getHealth());
                $this->consoleLogger->logInfo($this->defender->getName(). ' health:'.$this->defender->getHealth());
                $this->consoleLogger->logInfo($this->attacker->getName().' is the first attacker');
            }

            $damage = $this->attack();
            $this->defender->decreaseHealth($this->defend($damage));
            $this->consoleLogger->logAttack($this->attacker, $this->defender, $damage);

            $this->consoleLogger->logInfo('= Round '.$i.' ended');

            if (!$this->defender->isAlive()) {
                $winnerDeclared = true;
                $this->consoleLogger->logInfo($this->defender->getName().' died.');
                $this->consoleLogger->logInfo($this->attacker->getName().' won this game.');

                break;
            }

            // update states
            $this->attacker->setState(CharacterEnum::DEFEND);
            $this->defender->setState(CharacterEnum::ATTACK);
        }
        if (!$winnerDeclared){
            $this->consoleLogger->logInfo('Maximum number of rounds was reached. No winner declared');
        }
    }

    /**
     * @return int
     */
    private function attack(): int
    {
        $damage = $this->calculateDamage();
        if ($this->attacker->getType() !== CharacterEnum::SKILLED_CHARACTER) {
            return $damage;
        }

        $remainingAttacks = 0;
        $attackSkills = $this->getSkillsByType($this->attacker, CharacterEnum::ATTACK);
        foreach ($attackSkills as $skill) {
            if ($this->hasLuck($skill->getChance())) {
                $this->consoleLogger->logSkillUsage($this->attacker, $skill);
                $remainingAttacks += $skill->getValue();
            }
        }

        for ($i = 1; $i < $remainingAttacks; $i++){
            $damage += $this->calculateDamage();
        }

        return $damage;
    }

    private function defend(int $damage): int
    {
        if ($damage === 0) {
            $this->consoleLogger->logInfo($this->defender->getName().' was lucky this time');
            return 0;
        }
        if ($this->defender->getType() !== CharacterEnum::SKILLED_CHARACTER) {
            return $damage;
        }

        $defenceSkills = $this->getSkillsByType($this->defender, CharacterEnum::DEFENCE);
        foreach ($defenceSkills as $skill) {
            if ($this->hasLuck($skill->getChance())) {
                $this->consoleLogger->logSkillUsage($this->defender, $skill);
                $damage = $damage * $skill->getValue();
            }
        }

        return $damage;
    }

    private function calculateDamage(): int
    {
        if(!$this->hasLuck($this->defender->getLuck())) {
            return 0;
        }
        return $this->attacker->getStrength() - $this->defender->getDefence();
    }

    /**
     * @return bool
     */
    private function hasLuck(int $luck): bool
    {
        return rand(0, 100) < $luck;
    }

    /**
     * @param CharacterInterface $character
     * @return Skill[]
     */
    private function getSkillsByType(CharacterInterface $character, string $skillType): array
    {
        $skills = [];
        foreach ($character->getSkills() as $skill) {
            $skillTypeInterface = SkillEnum::SKILLS_BY_TYPE_MAPPING[$skillType];
            if ($skill instanceof $skillTypeInterface) {
                $skills[] = $skill;
            }
        }

        return $skills;
    }
}