<?php

namespace HeroGame\Strategy;

use HeroGame\Enum\CharacterEnum;
use HeroGame\Model\CharacterInterface;
use HeroGame\Model\Collection\CollectionInterface;

class SpeedAndLuckFirstAttackStrategy implements FirstAttack
{
    /**
     * @inheritDoc
     */
    public function setCharactersState(CollectionInterface $characters): void
    {
        /**
         * Ugly typehint, need to create CharacterCollection
         * @var CharacterInterface[] $charactersArray
         */
        $charactersArray = $characters->getAll();

        if ($charactersArray[0]->getSpeed() > $charactersArray[1]->getSpeed()) {
            $charactersArray[0]->setState(CharacterEnum::ATTACK);
            $charactersArray[1]->setState(CharacterEnum::DEFEND);

            return;
        }

        if ($charactersArray[0]->getSpeed() < $charactersArray[1]->getSpeed()) {
            $charactersArray[0]->setState(CharacterEnum::DEFEND);
            $charactersArray[1]->setState(CharacterEnum::ATTACK);

            return;
        }

        if ($charactersArray[0]->getLuck() > $charactersArray[1]->getLuck()) {
            $charactersArray[0]->setState(CharacterEnum::ATTACK);
            $charactersArray[1]->setState(CharacterEnum::DEFEND);

            return;
        }

        if ($charactersArray[0]->getLuck() < $charactersArray[1]->getLuck()) {
            $charactersArray[0]->setState(CharacterEnum::DEFEND);
            $charactersArray[1]->setState(CharacterEnum::ATTACK);

            return;
        }

        // the case of same luck was not caught in requirements.
        $charactersArray[0]->setState(CharacterEnum::ATTACK);
        $charactersArray[1]->setState(CharacterEnum::DEFEND);
    }
}