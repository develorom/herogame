<?php

namespace HeroGame\Strategy;

interface GameStrategyInterface
{
    public function runBattle(): void;
}