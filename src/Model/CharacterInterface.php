<?php

namespace HeroGame\Model;

interface CharacterInterface
{
    /**
     * @param int $damage
     *
     * @return int
     */
    public function decreaseHealth(int $damage): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return int
     */
    public function getSpeed(): int;

    /**
     * @return int
     */
    public function getHealth(): int;

    /**
     * @return int
     */
    public function getLuck(): int;

    /**
     * @return int
     */
    public function getDefence(): int;

    /**
     * @return bool
     */
    public function isAlive(): bool;

    /**
     * @return string
     */
    public function getState(): string;

    /**
     * @param string $state
     */
    public function setState(string $state): void;

    /**
     * @return string
     */
    public function getType(): string;
}