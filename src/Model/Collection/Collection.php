<?php

namespace HeroGame\Model\Collection;

class Collection implements CollectionInterface
{
    /**
     * @var CollectionItemInterface[]
     */
    protected array $items;

    /**
     * CollectionTest constructor.
     * @param CollectionItemInterface[] $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @param string $getter
     * @param string $value
     * @return CollectionItemInterface|null
     */
    public function getFirstByFieldValue(string $getter, string $value): ?CollectionItemInterface
    {
        foreach ($this->items as $item) {
            if (method_exists($item, $getter) && $item->$getter() === $value) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->items ?? [];
    }

    /**
     * @param int $index
     * @return CollectionItemInterface|null
     */
    public function get(int $index): ?CollectionItemInterface
    {
        return $this->items[$index] ?? null;
    }
}