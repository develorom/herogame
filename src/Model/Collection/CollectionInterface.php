<?php

namespace HeroGame\Model\Collection;

interface CollectionInterface
{
    /**
     * @param string $getter
     * @param string $value
     * @return CollectionItemInterface|null
     */
    public function getFirstByFieldValue(string $getter, string $value): ?CollectionItemInterface;

    /**
     * @return array
     */
    public function getAll(): array;

    /**
     * @param int $index
     * @return CollectionItemInterface|null
     */
    public function get(int $index): ?CollectionItemInterface;
}