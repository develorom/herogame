<?php

namespace HeroGame\Model;

use HeroGame\Enum\CharacterEnum;
use HeroGame\Exception\InvalidMoveException;
use HeroGame\Model\Collection\CollectionItemInterface;

class Character implements CharacterInterface, CollectionItemInterface
{
    private string $name;
    private int $health;
    private int $strength;
    private int $defence;
    private int $speed;
    private int $luck; // percentage

    private string $state = '';

    /**
     * Decrease health and return current health or 0 if character is dead
     *
     * @param int $damage
     *
     * @return int
     */
    public function decreaseHealth(int $damage): int
    {
        $this->health = $this->health - $damage;

        return $this->health > 0 ? $this->health : 0;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return CharacterEnum::CHARACTER;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Character
     */
    public function setName(string $name): Character
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health > 0 ? $this->health : 0;
    }

    /**
     * @param int $health
     *
     * @return Character
     */
    public function setHealth(int $health): Character
    {
        $this->health = $health;

        return $this;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     *
     * @return Character
     */
    public function setStrength(int $strength): Character
    {
        $this->strength = $strength;

        return $this;
    }

    /**
     * @return int
     */
    public function getDefence(): int
    {
        return $this->defence;
    }

    /**
     * @param int $defence
     *
     * @return Character
     */
    public function setDefence(int $defence): Character
    {
        $this->defence = $defence;

        return $this;
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * @param int $speed
     *
     * @return Character
     */
    public function setSpeed(int $speed): Character
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * @return int
     */
    public function getLuck(): int
    {
        return $this->luck;
    }

    /**
     * @param int $luck
     *
     * @return Character
     */
    public function setLuck(int $luck): Character
    {
        $this->luck = $luck;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAlive(): bool
    {
        return $this->getHealth() > 0;
    }

    /**
     * @inheritDoc
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @inheritDoc
     * @throws InvalidMoveException
     */
    public function setState(string $state): void
    {
        // this should be on validator part
        if (!in_array($state, CharacterEnum::VALIDATE_TRANSITIONS[$this->getState()])) {
            throw new InvalidMoveException('Invalid move!');
        }

        $this->state = $state;
    }
}