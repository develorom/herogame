<?php

namespace HeroGame\Model\Skill;

class RapidStrikeSkill extends Skill implements AttackSkillInterface
{
    /**
     * @return int
     */
    public function useForAttack(): int
    {
        if (!$this->hadChance()) {
            return 0;
        }

        return $this->getValue();
    }
}