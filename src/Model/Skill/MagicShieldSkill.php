<?php

namespace HeroGame\Model\Skill;

class MagicShieldSkill extends Skill implements DefenceSkillInterface
{
    /**
     * @param int $actualDamage
     * @return int
     */
    public function useForDefence(int $actualDamage): int
    {
        if (!$this->hadChance()) {
            return $actualDamage;
        }

        return $actualDamage / 2;
    }
}