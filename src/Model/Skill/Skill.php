<?php

namespace HeroGame\Model\Skill;

abstract class Skill
{
    private string $name;
    private string $type;
    private float $value;
    private int $chance;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Skill
     */
    public function setName(string $name): Skill
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Skill
     */
    public function setType(string $type): Skill
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @param float $value
     *
     * @return Skill
     */
    public function setValue(float $value): Skill
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getChance(): int
    {
        return $this->chance;
    }

    /**
     * @param int $chance
     *
     * @return Skill
     */
    public function setChance(int $chance): Skill
    {
        $this->chance = $chance;

        return $this;
    }

    /**
     * @return bool
     */
    protected function hadChance(): bool
    {
        return rand(0, 100) < $this->chance;
    }
}