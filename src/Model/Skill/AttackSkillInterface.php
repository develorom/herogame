<?php

namespace HeroGame\Model\Skill;

interface AttackSkillInterface
{
    /**
     * Return number of left attacks in case of success.
     *
     * @return int
     */
    public function useForAttack(): int;
}