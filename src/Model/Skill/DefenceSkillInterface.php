<?php

namespace HeroGame\Model\Skill;

interface DefenceSkillInterface
{
    /**
     * Receives current damage made by attacker and returns new damage in case of success
     *
     * @param int $hitPoints
     * @return int
     */
    public function useForDefence(int $actualDamage): int;
}