<?php

namespace HeroGame\Model;

use HeroGame\Enum\CharacterEnum;
use HeroGame\Model\Skill\Skill;

class SkilledCharacter extends Character
{
    /**
     * @var Skill[] $skills
     */
    private array $skills = [];

    /**
     * @return string
     */
    public function getType(): string
    {
        return CharacterEnum::SKILLED_CHARACTER;
    }

    /**
     * @param Skill[] $skills
     */
    public function setSkills(array $skills): void
    {
        $this->skills = $skills;
    }

    /**
     * @return Skill[]
     */
    public function getSkills(): array
    {
        return $this->skills;
    }
}