<?php

namespace HeroGame\Logger;

use HeroGame\Model\CharacterInterface;
use HeroGame\Model\Skill\Skill;

class ConsoleLogger implements HeroGameLoggerInterface
{
    /**
     * @inheritDoc
     */
    public function logAttack(CharacterInterface $attacker, CharacterInterface $defender, int $damage): void
    {
        echo $attacker->getName().' attacked '.$defender->getName().'. Damage done:'.$damage.
            ' Defender health left:'.$defender->getHealth().PHP_EOL;
    }

    /**
     * @inheritDoc
     */
    public function logSkillUsage(CharacterInterface $character, Skill $skill): void
    {
        echo $character->getName().' used skill '. $skill->getName().PHP_EOL;
    }

    /**
     * @inheritDoc
     */
    public function logInfo(string $message): void
    {
        echo $message.PHP_EOL;
    }
}