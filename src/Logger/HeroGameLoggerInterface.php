<?php

namespace HeroGame\Logger;

use HeroGame\Model\CharacterInterface;
use HeroGame\Model\Skill\Skill;

interface HeroGameLoggerInterface
{
    /**
     * @param CharacterInterface $attacker
     * @param CharacterInterface $defender
     * @param int $damage
     */
    public function logAttack(CharacterInterface $attacker, CharacterInterface $defender, int $damage): void;

    /**
     * @param CharacterInterface $character
     * @param Skill $skill
     */
    public function logSkillUsage(CharacterInterface $character, Skill $skill): void;

    /**
     * @param string $message
     */
    public function logInfo(string $message): void;
}