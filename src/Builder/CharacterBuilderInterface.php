<?php

namespace HeroGame\Builder;

use HeroGame\Model\CharacterInterface;

interface CharacterBuilderInterface
{
    public function build(array $characterConfig): CharacterInterface;
}