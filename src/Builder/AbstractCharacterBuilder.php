<?php

namespace HeroGame\Builder;

use HeroGame\Enum\CharacterEnum;
use HeroGame\Model\CharacterInterface;

abstract class AbstractCharacterBuilder implements CharacterBuilderInterface
{
    protected CharacterInterface $character;

    public abstract function build(array $characterConfig): CharacterInterface;

    protected function buildProperties(array $properties): void
    {
        $this->character->setHealth($this->generateValueInRange($properties[CharacterEnum::HEALTH]));
        $this->character->setStrength($this->generateValueInRange($properties[CharacterEnum::STRENGTH]));
        $this->character->setDefence($this->generateValueInRange($properties[CharacterEnum::DEFENCE]));
        $this->character->setSpeed($this->generateValueInRange($properties[CharacterEnum::SPEED]));
        $this->character->setLuck($this->generateValueInRange($properties[CharacterEnum::LUCK]));
    }

    /**
     * @param array $minMaxValues
     * @return int
     */
    private function generateValueInRange(array $minMaxValues): int
    {
        return rand($minMaxValues['min'], $minMaxValues['max']);
    }
}