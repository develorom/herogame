<?php

namespace HeroGame\Builder;

use HeroGame\Enum\CharacterEnum;
use HeroGame\Model\Character;

class CharacterBuilder extends AbstractCharacterBuilder
{
    /**
     * CharacterBuilder constructor.
     */
    public function __construct()
    {
        $this->character = new Character();
    }

    /**
     * @param array $characterConfig
     * @return Character
     */
    public function build(array $characterConfig): Character
    {
        $this->character->setName($characterConfig[CharacterEnum::NAME]);
        $this->buildProperties($characterConfig[CharacterEnum::PROPERTIES]);

        return $this->character;
    }
}