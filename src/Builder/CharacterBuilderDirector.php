<?php

namespace HeroGame\Builder;

use HeroGame\Enum\CharacterEnum;
use HeroGame\Model\CharacterInterface;

class CharacterBuilderDirector
{
    public const CHARACTERS_CONFIG_KEY = 'characters';

    public function build(array $characterConfig): CharacterInterface
    {
        if (CharacterEnum::SKILLED_CHARACTER === $characterConfig['class']) {
            return (new SkilledCharacterBuilder())->build($characterConfig);
        }

        return (new CharacterBuilder())->build($characterConfig);
    }
}