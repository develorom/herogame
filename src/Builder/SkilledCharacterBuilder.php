<?php

namespace HeroGame\Builder;

use HeroGame\Enum\CharacterEnum;
use HeroGame\Enum\SkillEnum;
use HeroGame\Factory\SkillFactory;
use HeroGame\Model\SkilledCharacter;

class SkilledCharacterBuilder extends AbstractCharacterBuilder
{
    private SkillFactory $skillFactory;

    public function __construct()
    {
        $this->character = new SkilledCharacter();
        $this->skillFactory = new SkillFactory();
    }

    /**
     * @param array $characterConfig
     * @return SkilledCharacter
     */
    public function build(array $characterConfig): SkilledCharacter
    {
        $this->character->setName($characterConfig[CharacterEnum::NAME]);
        $this->buildProperties($characterConfig[CharacterEnum::PROPERTIES]);
        $this->character->setSkills(
            $this->buildSkills($characterConfig[CharacterEnum::SKILLS])
        );

        return $this->character;
    }

    /**
     * @param array $skillsConfig
     * @return array
     */
    private function buildSkills(array $skillsConfig): array
    {
        $skills = [];

        foreach ($skillsConfig as $skillConfig) {
            $skill = $this->skillFactory->createInstance($skillConfig[SkillEnum::CLASS_FIELD]);
            $skill->setName($skillConfig[SkillEnum::PROPERTIES][SkillEnum::NAME]);
            $skill->setType($skillConfig[SkillEnum::PROPERTIES][SkillEnum::TYPE]);
            $skill->setValue($skillConfig[SkillEnum::PROPERTIES][SkillEnum::VALUE]);
            $skill->setChance($skillConfig[SkillEnum::PROPERTIES][SkillEnum::CHANCE]);
            $skills[] = $skill;
        }

        return $skills;
    }
}