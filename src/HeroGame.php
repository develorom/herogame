<?php

namespace HeroGame;

use HeroGame\Builder\CharacterBuilderDirector;
use HeroGame\Enum\GameplayEnum;
use HeroGame\Factory\GameStrategyFactory;
use HeroGame\Logger\ConsoleLogger;
use HeroGame\Model\Collection\Collection;
use HeroGame\Strategy\GameStrategyInterface;
use HeroGame\Strategy\SpeedAndLuckFirstAttackStrategy;

class HeroGame
{
    private array $config;
    private CharacterBuilderDirector $characterBuilderDirector;
    private ConsoleLogger $consoleLogger;
    private GameStrategyFactory $gameStrategyFactory;

    /**
     * HeroGame constructor.
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->characterBuilderDirector = new CharacterBuilderDirector();
        $this->consoleLogger = new ConsoleLogger();
        $this->gameStrategyFactory = new GameStrategyFactory();
    }

    public function run(): void
    {
        // build characters
        $characters = [];
        foreach ($this->config[CharacterBuilderDirector::CHARACTERS_CONFIG_KEY] as $characterConfig) {
            $characters[] = $this->characterBuilderDirector->build($characterConfig);
        }

        $characters = new Collection($characters); // todo: add() method.

        // check first attacker
        (new SpeedAndLuckFirstAttackStrategy())->setCharactersState($characters);

        $this->consoleLogger->logInfo('Game started');

        $maxTurns = $this->config[GameplayEnum::GAMEPLAY][GameplayEnum::MAX_TURNS];
        $this->gameStrategyFactory->build($characters, $maxTurns)->runBattle();

        $this->consoleLogger->logInfo('Game ended');
    }
}