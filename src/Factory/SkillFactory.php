<?php

namespace HeroGame\Factory;

use HeroGame\Enum\SkillEnum;
use HeroGame\Exception\InvalidSkillException;
use HeroGame\Model\Skill\Skill;

class SkillFactory
{
    /**
     * @param string $className
     * @return Skill
     * @throws InvalidSkillException
     */
    public function createInstance(string $className): Skill
    {
        $class = SkillEnum::SKILL_FACTORY_CLASS_MAPPING[$className] ?? null;

        if (null === $class) {
            throw new InvalidSkillException('Skill type not found in class mapping');
        }
        return new $class();
    }
}