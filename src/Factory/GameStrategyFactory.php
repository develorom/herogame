<?php

namespace HeroGame\Factory;

use HeroGame\Enum\GameplayEnum;
use HeroGame\Exception\HeroGameException;
use HeroGame\Model\Collection\CollectionInterface;
use HeroGame\Strategy\GameStrategyInterface;

class GameStrategyFactory
{
    /**
     * @param CollectionInterface $characters
     * @param                     $maxTurns
     * @return GameStrategyInterface
     * @throws HeroGameException
     */
    public function build(CollectionInterface $characters, $maxTurns): GameStrategyInterface
    {
        $class = GameplayEnum::GAMEPLAY_STRATEGY_BY_CHARACTERS_NUMBER[count($characters->getAll())] ?? null;

        if (null === $class) {
            throw new HeroGameException('Gameplay not found in class mapping');
        }

        return new $class($characters, $maxTurns); // ugly
    }
}