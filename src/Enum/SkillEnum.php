<?php

namespace HeroGame\Enum;

use HeroGame\Model\Skill\AttackSkillInterface;
use HeroGame\Model\Skill\DefenceSkillInterface;
use HeroGame\Model\Skill\MagicShieldSkill;
use HeroGame\Model\Skill\RapidStrikeSkill;

class SkillEnum
{
    public const CLASS_FIELD = 'class'; // suffixed because CLASS is reserved word.

    public const PROPERTIES = 'properties';
    public const NAME = 'name';
    public const TYPE = 'type';
    public const VALUE = 'value';
    public const CHANCE = 'chance';

    public const RAPID_STRIKE = 'RapidStrike';
    public const MAGIC_SHIELD = 'MagicShield';

    public const SKILL_FACTORY_CLASS_MAPPING = [
        self::RAPID_STRIKE => RapidStrikeSkill::class,
        self::MAGIC_SHIELD => MagicShieldSkill::class,
    ];

    public const SKILLS_BY_TYPE_MAPPING = [
        CharacterEnum::ATTACK => AttackSkillInterface::class,
        CharacterEnum::DEFENCE => DefenceSkillInterface::class,
    ];
}