<?php

namespace HeroGame\Enum;

use HeroGame\Strategy\PvPGameStrategy;

class GameplayEnum
{
    public const GAMEPLAY = 'gameplay';
    public const MAX_TURNS = 'maxTurns';

    public const GAMEPLAY_STRATEGY_BY_CHARACTERS_NUMBER = [
        2 => PvPGameStrategy::class,
    ];
}