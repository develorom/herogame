<?php

namespace HeroGame\Enum;

use HeroGame\Model\Character;
use HeroGame\Model\SkilledCharacter;

class CharacterEnum
{
    public const SKILLED_CHARACTER = 'SkilledCharacter';
    public const CHARACTER = 'Character';

    public const PROPERTIES = 'properties';
    public const NAME = 'name';
    public const HEALTH = 'health';
    public const STRENGTH = 'strength';
    public const DEFENCE = 'defence';
    public const SPEED = 'speed';
    public const LUCK = 'luck';

    public const SKILLS = 'skills';

    public const ATTACK = 'attack';
    public const DEFEND = 'defend';
    public const INITIAL_STATE = '';

    public const VALIDATE_TRANSITIONS = [
        self::INITIAL_STATE => [
            self::ATTACK,
            self::DEFEND,
        ],
        self::ATTACK => [
            self::DEFEND,
        ],
        self::DEFEND => [
            self::ATTACK,
        ],
    ];
}