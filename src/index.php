#!/usr/bin/env php
<?php

require __DIR__ . '/../vendor/autoload.php';

use HeroGame\HeroGame;

try {
    $config = \yaml_parse_file(__DIR__.'/config.yaml');
    $game = new HeroGame($config);
    $game->run();
} catch (\Exception $exception) {
    echo $exception->getMessage();
}

