# eMag - HeroGame

### Installation & running
I assume you already have `Docker` and `docker-compose` installed.
1. Start the containers `docker-compose up -d` from current path
2. Open container `docker exec -ti herogame_php_1 bash`
3. Install dependencies `composr install` from container
4. Run it `php index.php`

### Running tests
Run `./vendor/bin/phpunit tests` from container. Also I created a coverage report `tests-coverage/dashboard.html`
