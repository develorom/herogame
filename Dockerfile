FROM php:7.4

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN apt-get update && apt-get install -y zip \
     unzip \
     libzip-dev \
     libyaml-dev \
     --no-install-recommends \
     && docker-php-ext-install zip

RUN pecl install xdebug-2.9.3 && docker-php-ext-enable xdebug
RUN pecl install yaml && docker-php-ext-enable yaml

ADD ./config/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

WORKDIR /home/app

EXPOSE 9000