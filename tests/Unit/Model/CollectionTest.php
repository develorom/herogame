<?php

namespace HeroGame\tests\Unit\Model;

use HeroGame\Model\Character;
use HeroGame\Model\Collection\Collection;
use HeroGame\Model\Collection\CollectionItemInterface;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    private Collection $collection;

    public function setUp(): void
    {
        $this->collection = new Collection($this->prepareCollectionItems());
    }

    public function testGetAll(): void
    {
        $this->assertEquals(2, count($this->collection->getAll()));
    }

    public function testGetFirstByFieldValue(): void
    {
        $item = $this->collection->getFirstByFieldValue('getState', 'attack');
        $this->assertEquals('first', $item->getName());
    }

    public function testGetByIndex(): void
    {
        $item = $this->collection->get(0);
        $this->assertEquals('first', $item->getName());
    }

    /**
     * @return CollectionItemInterface[]
     */
    private function prepareCollectionItems(): array
    {
        $item1 = new Character();
        $item1->setName('first');
        $item1->setState('attack');

        $item2 = new Character();
        $item2->setName('second');
        $item2->setState('defend');

        $items[] = $item1;
        $items[] = $item2;

        return $items;
    }
}