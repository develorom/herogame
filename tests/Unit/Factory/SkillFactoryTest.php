<?php

namespace HeroGame\tests\Unit\Factory;

use HeroGame\Exception\InvalidSkillException;
use HeroGame\Factory\SkillFactory;
use HeroGame\Model\Skill\MagicShieldSkill;
use HeroGame\Model\Skill\RapidStrikeSkill;
use PHPUnit\Framework\TestCase;

class SkillFactoryTest extends TestCase
{
    private SkillFactory $skillFactory;

    public function setUp(): void
    {
        $this->skillFactory = new SkillFactory();
    }

    /**
     * @dataProvider createSkillByTypeDataProvider
     */
    public function testCreateSkillByType($skillType, $expectedClass): void
    {
        $skill = $this->skillFactory->createInstance($skillType);
        $this->assertInstanceOf($expectedClass, $skill);
    }

    public function testCreatingUnmappedSkillThrowsError(): void
    {
        $this->expectException(InvalidSkillException::class);
        $this->skillFactory->createInstance('unmapped-skill');
    }

    /**
     * @return array
     */
    public function createSkillByTypeDataProvider(): array
    {
        return [
            ['RapidStrike', RapidStrikeSkill::class],
            ['MagicShield', MagicShieldSkill::class],
        ];
    }
}