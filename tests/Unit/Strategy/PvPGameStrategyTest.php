<?php

namespace HeroGame\tests\Strategy;

use HeroGame\Logger\ConsoleLogger;
use HeroGame\Model\Character;
use HeroGame\Model\CharacterInterface;
use HeroGame\Model\Collection\Collection;
use HeroGame\Strategy\PvPGameStrategy;
use HeroGame\tests\Helper\ReflectionHelper;
use PHPUnit\Framework\TestCase;

class PvPGameStrategyTest extends TestCase
{
    private PvPGameStrategy $gameStrategy;
    private $consoleLogger;

    public function setUp(): void
    {
        $characters = new Collection([]);
        $this->gameStrategy = new PvPGameStrategy($characters, 20);
        $this->consoleLogger = $this->createMock(ConsoleLogger::class);

        // this was avoided if used dependency injection and ConsoleLogger was injected in GameStrategy constructor
        ReflectionHelper::setPrivateProperty($this->gameStrategy, 'consoleLogger', $this->consoleLogger);
    }

    public function testDefendNoDamage(): void
    {
        $defenderMock = $this->createMock(Character::class);
        $defenderMock->method('getName')->willReturn('Defender');

        ReflectionHelper::setPrivateProperty($this->gameStrategy, 'defender', $defenderMock);

        $this->consoleLogger->expects($this->once())
            ->method('logInfo')
            ->with('Defender was lucky this time');

        $damage = ReflectionHelper::callPrivateMethod($this->gameStrategy, 'defend', [0]);
        $this->assertEquals(0, $damage);
    }
}