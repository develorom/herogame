<?php

namespace HeroGame\tests\Helper;

class ReflectionHelper
{
    /**
     * @param object $object
     * @param string $property
     * @param mixed $value
     * @throws \ReflectionException
     */
    public static function setPrivateProperty(object $object, string $property, $value): void
    {
        $reflection = new \ReflectionClass($object);
        $property = $reflection->getProperty($property);
        $property->setAccessible(true);
        $property->setValue($object, $value);
    }

    /**
     * @param object $object
     */
    public static function callPrivateMethod(object $object, string $method, array $args)
    {
        $reflection = new \ReflectionClass($object);
        $method = $reflection->getMethod($method);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $args);
    }
}